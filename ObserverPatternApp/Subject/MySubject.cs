﻿using ObserverPatternApp.Observer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPatternApp.Subject
{
    interface MySubject
    {
        void RegisterObserver(MyObserver observer);
        void UnregisterObserver(MyObserver observer);
        void NotifyObserver(int count);
    }
}
