﻿namespace ObserverPatternApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonincrease = new System.Windows.Forms.Button();
            this.buttondecrease = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonincrease
            // 
            this.buttonincrease.Location = new System.Drawing.Point(31, 81);
            this.buttonincrease.Name = "buttonincrease";
            this.buttonincrease.Size = new System.Drawing.Size(121, 63);
            this.buttonincrease.TabIndex = 0;
            this.buttonincrease.Text = "INCREASE( + )";
            this.buttonincrease.UseVisualStyleBackColor = true;
            this.buttonincrease.Click += new System.EventHandler(this.buttonincrease_Click);
            // 
            // buttondecrease
            // 
            this.buttondecrease.Location = new System.Drawing.Point(258, 81);
            this.buttondecrease.Name = "buttondecrease";
            this.buttondecrease.Size = new System.Drawing.Size(113, 63);
            this.buttondecrease.TabIndex = 1;
            this.buttondecrease.Text = "DECREASE( - )";
            this.buttondecrease.UseVisualStyleBackColor = true;
            this.buttondecrease.Click += new System.EventHandler(this.buttondecrease_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 244);
            this.Controls.Add(this.buttondecrease);
            this.Controls.Add(this.buttonincrease);
            this.Name = "MainForm";
            this.Text = "Control Window";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonincrease;
        private System.Windows.Forms.Button buttondecrease;
    }
}

