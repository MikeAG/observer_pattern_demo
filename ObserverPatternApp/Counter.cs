﻿using ObserverPatternApp.Observer;
using ObserverPatternApp.Subject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPatternApp
{
    public class Counter : MySubject
    {
        private List<MyObserver> observers;
        private int count;

        public Counter()
        {
            observers = new List<MyObserver>();
            count = 0;
        }

        public void Increment()
        {
            count++;
            NotifyObserver(count);
        }


        public void Decrement()
        {
            if (count > 0)
            {
                count--;
                NotifyObserver(count);
            }

        }




        public void NotifyObserver(int count)
        {
            foreach (MyObserver ob in observers)
            {
                ob.Update(count);
            }
        }

        public void RegisterObserver(MyObserver observer)
        {
            observers.Add(observer);
        }

        public void UnregisterObserver(MyObserver observer)
        {
            observers.Remove(observer);
        }
    }
}
