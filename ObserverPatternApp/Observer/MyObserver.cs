﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPatternApp.Observer
{
    public interface MyObserver
    {
        void Update(int count);

    }
}
