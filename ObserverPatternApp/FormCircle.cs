﻿using ObserverPatternApp.Observer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObserverPatternApp
{
    public partial class circleobserver : Form, MyObserver
    {
        public circleobserver()
        {
            InitializeComponent();
        }

        public void Update(int count)
        {
            updatecircle(count);
        }

        private void updatecircle(int count)
        {
            this.CreateGraphics().Clear(this.BackColor);
            Graphics g = this.CreateGraphics();
            Pen pen = new Pen(Color.Red);
            g.DrawEllipse(pen, 10, 10, count * 10, count * 10);
        }
    }
}
