﻿using ObserverPatternApp.Observer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObserverPatternApp
{
    public partial class FormRectangle : Form, MyObserver
    {
        public FormRectangle()
        {
            InitializeComponent();
        }

        public void Update(int count)
        {
            updateRectangle(count);
        }

        private void updateRectangle(int count)
        {
            this.CreateGraphics().Clear(this.BackColor);
            System.Drawing.SolidBrush mybrush = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            Graphics formgraphics;
            formgraphics = this.CreateGraphics();

            formgraphics.FillRectangle(mybrush, new Rectangle(0, 0, count * 10, count * 10));
            mybrush.Dispose();
            formgraphics.Dispose();
        }
    }
}
