﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObserverPatternApp
{
    public partial class MainForm : Form
    {
        private Counter counter;

        public MainForm()
        {
            InitializeComponent();
            counter = new Counter();


            FormText formtext = new FormText();
            formtext.Show();
            counter.RegisterObserver(formtext);

            FormRectangle formRec = new FormRectangle();
            formRec.Show();
            counter.RegisterObserver(formRec);

            circleobserver co = new circleobserver();
            co.Show();
            counter.RegisterObserver(co);

            
        }

        private void buttonincrease_Click(object sender, EventArgs e)
        {
            counter.Increment();
        }

        private void buttondecrease_Click(object sender, EventArgs e)
        {
            counter.Decrement();
        }
    }
}
