﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ObserverPatternApp.Observer;


namespace ObserverPatternApp
{
    public partial class FormText : Form, MyObserver
    {


        public FormText()
        {

            InitializeComponent();


        }

        public void Update(int count)
        {
            setText(count);
        }

        private void setText(int count)
        {
            labeltext.Text = count.ToString();
        }

        private void label1_Click(object sender, EventArgs e)
        {


        }
    }
}
